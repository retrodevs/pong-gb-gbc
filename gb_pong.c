#include <gb/gb.h>
#include <stdio.h>
#include <gb/drawing.h>
#include <gb/console.h>

#define TRUE 1
#define FALSE 0

#define SHORT_LENGTH 15
#define MEDIUM_LENGTH 25
#define LONG_LENGTH 35

#define PLAYER1_X 5
#define PLAYER2_X 153

#define SCORE1_X 32
#define SCORE1_Y 20
#define SCORE2_X 120
#define SCORE2_Y 20

#define MOVE_LENGTH 10

void clear_screen(); //Redraw the background
void draw_players();
void draw_score();

typedef struct ball{
	unsigned char x;
	unsigned char y;
} Ball;

typedef struct player{
	unsigned char y; //Height of racket
	unsigned char length; //Lenght of the racket
} Player;

//Time sleep to wait next ball position
int speed=50;
//1 byte for score. First 4bits player 1 - last 4bits player 2 (MAX score= 2^4-1 = 15)
char score=0;
//players' rackets
Player player1={55,MEDIUM_LENGTH};
Player player2={55,MEDIUM_LENGTH};


void main(){
	UINT8 keys;
	//unsigned char temp;
	//Round array to draw the sequence path of tha ball
	Ball ball_sequence[5]={{GRAPHICS_WIDTH-1,GRAPHICS_HEIGHT-1},{255,255},{255,255},{255,255},{255,255}};
	//I don't know why, but need to redrwa two times the first time to work
	clear_screen();
	clear_screen();
	draw_players();
	draw_score();
	while(TRUE){
		keys=joypad();
		if((keys & J_UP)){ // UP è stato premuto
			if(player1.y>MOVE_LENGTH){
				color(BLACK,BLACK,SOLID);
				box(PLAYER1_X,player1.y+player1.length-MOVE_LENGTH,PLAYER1_X+1,player1.y+player1.length,M_FILL);
				player1.y-=MOVE_LENGTH;
				color(WHITE,WHITE,SOLID);
				box(PLAYER1_X,player1.y,PLAYER1_X+1,player1.y+MOVE_LENGTH,M_FILL);
			}else{
				color(BLACK,BLACK,SOLID);
				box(PLAYER1_X,player1.length,PLAYER1_X+1,player1.y+player1.length,M_FILL);
				color(WHITE,WHITE,SOLID);
				box(PLAYER1_X,0,PLAYER1_X+1,player1.y,M_FILL);
				player1.y=0;
			}
        }else if((keys & J_DOWN) && player1.y+player1.length<GRAPHICS_HEIGHT){ // UP è stato premuto
			color(BLACK,BLACK,SOLID);
            box(PLAYER1_X,player1.y,PLAYER1_X+1,player1.y+MOVE_LENGTH,M_FILL);
			player1.y+=MOVE_LENGTH;
            color(WHITE,WHITE,SOLID);
            box(PLAYER1_X,player1.y+player1.length-MOVE_LENGTH,PLAYER1_X+1,player1.y+player1.length,M_FILL);
        }
		if((keys & J_A)){ // UP è stato premuto
			if(player2.y>MOVE_LENGTH){
				color(BLACK,BLACK,SOLID);
				box(PLAYER2_X,player2.y+player2.length-MOVE_LENGTH,PLAYER2_X+1,player2.y+player2.length,M_FILL);
				player2.y-=MOVE_LENGTH;
				color(WHITE,WHITE,SOLID);
				box(PLAYER2_X,player2.y,PLAYER2_X+1,player2.y+MOVE_LENGTH,M_FILL);
			}else{
				color(BLACK,BLACK,SOLID);
				box(PLAYER2_X,player2.length,PLAYER2_X+1,player2.y+player2.length,M_FILL);
				color(WHITE,WHITE,SOLID);
				box(PLAYER2_X,0,PLAYER2_X+1,player2.y,M_FILL);
				player2.y=0;
			}
        }else if((keys & J_B) && player2.y+player2.length<GRAPHICS_HEIGHT){ // UP è stato premuto
			color(BLACK,BLACK,SOLID);
            box(PLAYER2_X,player2.y,PLAYER2_X+1,player2.y+MOVE_LENGTH,M_FILL);
			player2.y+=MOVE_LENGTH;
            color(WHITE,WHITE,SOLID);
            box(PLAYER2_X,player2.y+player2.length-MOVE_LENGTH,PLAYER2_X+1,player2.y+player2.length,M_FILL);
        }
		delay(speed);
	}
}

// Function to redraw the basic background of the game:
// Draws a rectangle of same screen dimensions with
// border and body(FILL) BLACK plus middle WHITE lines
void clear_screen(){
	unsigned char line_leng=15,y=4;
    color(BLACK,BLACK,SOLID);
    box(0,0,GRAPHICS_WIDTH-1,GRAPHICS_HEIGHT-1,M_FILL);
    color(WHITE,WHITE,SOLID);
	for(;y<GRAPHICS_HEIGHT;y+=line_leng*2){
		box(GRAPHICS_WIDTH/2,y,GRAPHICS_WIDTH/2+1,y+line_leng,M_FILL);
	}
}

//Draw the players' rackets
void draw_players(){
    color(WHITE,WHITE,SOLID);
	box(PLAYER1_X,player1.y,PLAYER1_X+1,player1.y+player1.length,M_FILL);
	box(PLAYER2_X,player2.y,PLAYER2_X+1,player2.y+player2.length,M_FILL);
}

//Draw the actual scores
void draw_score(){
	color(WHITE,BLACK,SOLID);
	gotogxy(SCORE1_X/8, SCORE1_Y/8);
	gprintn(score>>4, 10, UNSIGNED);
	gotogxy(SCORE2_X/8, SCORE2_Y/8);
	gprintn(score & 0x0f, 10, UNSIGNED);
}